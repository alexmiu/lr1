#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
def main():
print("Hello, World!")
print("This is {script_name}".format(script_name=sys.argv[0]))
if __name__ == "__main__":
main()
